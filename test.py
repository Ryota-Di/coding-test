x = []
while True:
    line = input()
    if ":" in line:
        x.append(list(map(str,line.split(":"))))
    else:
        m = int(line)
        break
x = sorted(x,key=lambda x:int(x[0]))
res = ""
for i in x:
    if m%int(i[0])==0:
        res += i[1]
if res == "":
    print(m)
else:
    print(res)
